package cql.ecci.ucr.ac.b61144_ejemplo_2_1;

public class Tip {
    public static final String IMAGEN = "i";
    private final String name;
    private final String img;
    private final String description;
    public Tip(String name, String img, String description) {
        this.name = name;
        this.img = img;
        this.description = description;
    }
    public String getName() {
        return name;
    }
    public String getImg() {
        return img;
    }
    public String getDescription() {
        return description;
    }
}
